var score;
var playing = false ;
var timeremaining;
var countdown;
var correctAns;
let x=0;
let y=0;

/*HELPER*/
function setText(id, text){
	document.getElementById(id).innerHTML = text;
}

function show(id){
	document.getElementById(id).style.display = 'block';
	
}
function hide(id){
	document.getElementById(id).style.display='none';
}

document.getElementById("startreset").onclick = function(){
	if(playing == true){
		// game is on and you tried to test it!!
		playing = false;
		window.location.reload();
	}else{
		// game is off and you want to start it
		playing = true;
		
		score = 0;
		setText("scoreValue", score);
		
		show("timeremaining");
		timeremaining = 20;
		setText("timeremainingValue", timeremaining);
		
		this.innerHTML = "Reset Game";
		hide("gameover");
		startCountdown();
		generateQA();
	}
}

function startCountdown(){
	countdown = setInterval(function(){
		timeremaining -=1;
		setText("timeremainingValue",timeremaining);
		if(timeremaining<=0){
			stopCountdown();
			show("gameover");
			playing = false;
			setText("startreset", "Start Game!");
			hide("timeremaining");
			setText("scoreValue", "");
			setText("gameover" , "<p> Game Over ! </p> <p> Your score is " + score + "</p>")
		}
	},1000);
}

function stopCountdown(){
	clearInterval(countdown);
}

function generateQA(){
	 x = (1+ Math.round(Math.random() * 9));
	 y = (1+ Math.round(Math.random() * 9));
	
	var count =(1+ Math.round(Math.random()*3));
	
	if(count == 1){
		addNos();	
	}else if(count == 2){
		subNos();
		
	}else if(count == 3){
		mulNos();
	}else if (count == 4){
		divNos();		
}

}
	function addNos(){
	correctAns = x+y;
		setText("question" , x + "+" +y);
		var correctPosition = (1+ Math.round(Math.random() * 3));
		setText("box" + correctPosition,correctAns);
	
			var answers = [correctAns];
			for(i=1;i<5;i++){
				var wrongAns;
				if(i!=correctPosition){
					do{
						wrongAns = (1+ Math.round(Math.random() * 9)) + (1+ Math.round(Math.random() * 9));
					}while(answers.indexOf(wrongAns)>-1);

					answers.push(wrongAns);
					setText("box" + i , wrongAns);
				}
			}
}
function subNos(){
	correctAns = x-y;
		setText("question" , x + "-" +y);
		var correctPosition = (1+ Math.round(Math.random() * 3));
		setText("box" + correctPosition,correctAns);
	
			var answers = [correctAns];
			for(i=1;i<5;i++){
				var wrongAns;
				if(i!=correctPosition){
					do{
						wrongAns = (1+ Math.round(Math.random() * 9)) - (1+ Math.round(Math.random() * 9));
					}while(answers.indexOf(wrongAns)>-1);

					answers.push(wrongAns);
					setText("box" + i , wrongAns);
				}
			}
}

function mulNos(){
	correctAns = x*y;
		setText("question" , x + "*" +y);
		var correctPosition = (1+ Math.round(Math.random() * 3));
		setText("box" + correctPosition,correctAns);
	
			var answers = [correctAns];
			for(i=1;i<5;i++){
				var wrongAns;
				if(i!=correctPosition){
					do{
						wrongAns = (1+ Math.round(Math.random() * 9)) * (1+ Math.round(Math.random() * 9));
					}while(answers.indexOf(wrongAns)>-1);

					answers.push(wrongAns);
					setText("box" + i , wrongAns);
				}
			}
}
	function divNos(){
	correctAns = x/y;
	correctAns=correctAns.toFixed(2);
	setText("question" , x + "/" +y);
	
	var correctPosition = (1+ Math.round(Math.random() * 3));
	setText("box" + correctPosition,correctAns);
	
	var answers = [correctAns];
	for(i=1;i<5;i++){
		var wrongAns;
		if(i!=correctPosition){
			do{
				wrongAns = Math.round((1+ Math.round(Math.random() * 9)) / (1+ Math.round(Math.random() * 9)));
				wrongAns = wrongAns.toFixed(2);
			}while(answers.indexOf(wrongAns)>-1);
			
			answers.push(wrongAns);
			setText("box" + i , wrongAns);
		}
	}
	}
for(i=1;i<5;i++){
	document.getElementById("box"+i).onclick = function(){
		if(playing){
			if(correctAns == this.innerHTML){
				score++;
				setText("scoreValue",score);
				show("correct");
				hide("wrong");
				setTimeout(function(){
					hide("correct");
				},1000);
				
				generateQA();
			}else{
				show("wrong");
				hide("correct");
				setTimeout(function(){
					hide("wrong");
				},1000);
			}
		}
	}
}